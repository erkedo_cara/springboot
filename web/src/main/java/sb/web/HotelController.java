package sb.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import s.domain.Hotel;
import sb.persistence.HotelRepozitory;

import java.util.List;

@RestController
public class HotelController {

    private final HotelRepozitory hotelRepozitory;

    public HotelController(HotelRepozitory hotelRepozitory) {
        this.hotelRepozitory = hotelRepozitory;
    }

    @GetMapping(value = "/hotels")
    public List<Hotel> getHotels(){
        List<Hotel> hotels = hotelRepozitory.findAll();
        return hotels;

    }

}
