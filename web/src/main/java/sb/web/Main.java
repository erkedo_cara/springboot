package sb.web;


import com.sun.swing.internal.plaf.basic.resources.basic_es;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = "sb.persistence.component")
@EnableJpaRepositories(basePackages = "sb.persistence")
@EntityScan(basePackages = "s.domain")
@ComponentScan

public class Main {

    public static void main(String[] args){
        SpringApplication.run(Main.class);
    }
}
