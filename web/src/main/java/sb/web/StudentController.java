package sb.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import s.domain.Student;
import sb.persistence.component.StudentRepository;

import java.util.List;

@RestController
public class StudentController {

    @Autowired
    private StudentRepository studentRepository;

    @GetMapping(value = "/students")
    public List<Student> getStudents(){
        List<Student> studentList = studentRepository.findAll();
        return studentList;
    }
}
