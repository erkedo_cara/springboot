package sb.persistence.component;


import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import s.domain.Hotel;
import sb.persistence.HotelRepozitory;

import java.util.ArrayList;
import java.util.List;

@Component
public class DbSeeder implements CommandLineRunner {
    private final HotelRepozitory hotelRepozitory;

    public DbSeeder(HotelRepozitory hotelRepozitory) {
        this.hotelRepozitory = hotelRepozitory;
    }

    @Override
    public void run(String... args) throws Exception {
        Hotel mariot = new Hotel("mariot",5,true);
        Hotel ibis = new Hotel("erkedo",2,false);
        List<Hotel> listOfHotels = new ArrayList<>();
        listOfHotels.add(mariot);
        listOfHotels.add(ibis);
        this.hotelRepozitory.saveAll(listOfHotels);

    }

}
