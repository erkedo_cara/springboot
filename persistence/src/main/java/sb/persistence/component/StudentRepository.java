package sb.persistence.component;

import org.springframework.data.jpa.repository.JpaRepository;
import s.domain.Student;

public interface StudentRepository extends JpaRepository<Student,Long> {

}
