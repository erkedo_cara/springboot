package sb.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import s.domain.Hotel;
import s.domain.Student;

@Repository
public interface HotelRepozitory extends JpaRepository<Hotel,Long> {

}
