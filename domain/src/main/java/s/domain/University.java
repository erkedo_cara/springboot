package s.domain;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class University {

    String name;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @OneToMany
    @JoinTable(name = "university_course_students", joinColumns = @JoinColumn(name = "university_id"), inverseJoinColumns = @JoinColumn(name = "student_id"))
    private Collection<Student> students;
    @OneToMany
    @JoinTable(name = "university_course_students", joinColumns = @JoinColumn(name = "university_id"), inverseJoinColumns = @JoinColumn(name = "course_id"))
    private Collection<Course> courses;
    @OneToMany
    @JoinTable(name = "university_course_students", joinColumns = @JoinColumn(name = "university_id"), inverseJoinColumns = @JoinColumn(name = "instructor_id"))
    private Collection<Instructor> instructors;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Collection<Student> getStudents() {
        return students;
    }

    public void setStudents(Collection<Student> students) {
        this.students = students;
    }

    public Collection<Course> getCourses() {
        return courses;
    }

    public void setCourses(Collection<Course> courses) {
        this.courses = courses;
    }

    public Collection<Instructor> getInstructors() {
        return instructors;
    }

    public void setInstructors(Collection<Instructor> instructors) {
        this.instructors = instructors;
    }
}
